INTRODUCTION
------------

The Duplicate file fixer module finds and, optionally, fixes duplicate
file entries on your Drupal database.


BACKGROUND
----------

Drupal uses a database table to keep track of the files used on your
site. In this table, file paths are supposed to be unique but, for
some unknown reasons, sometimes two or more file entries with the same
file path are generated on the database.

Usually, this does not pose any problem to a Drupal 6 or below
site. Your can run your D6 site forever without encountering any
problem related to those duplicated entries.

But beginning with Drupal 7, file paths *must be unique* on the
database. This *will* pose a problem if your try to update your site
to Drupal 7, and it happens to have duplicated file entries. In fact,
the Drupal 7 upgrade script will fail badly when handling this
situation, leaving you with a half baked update.

See https://www.drupal.org/node/1260938 for a more in-depth discussion
about this problem.


WHAT THIS MODULE DOES
---------------------

This module provides a report in Reports > Duplicate file entries
(admin/reports/d6dupfix) that list all file entries that were found to
be duplicated, along with additional information that may be useful to
manually fix the issue (for example, you can delete and old node
revision to get rid of a duplicate).

You are also offered a chance to let the module fix the issue by
itself. To do this, just select the entries your want to fix and click
"Fix all selected entries" on the bottom.

For every selected file, the following operations are done:

    1. All duplicate entries are collected. The first entry is not touched,
       so that the original file is left unchanged.

    2. For all subsequent entries, a new uniquely file name is
       generated, and a hard link is created, pointing to the first
       entry.

    3. The database record of the duplicated entry is updated to point
       to the new (unique) file name.


REQUIREMENTS
------------

No special requirements.


INSTALLATION
------------

Install as you would normally install a contributed Drupal module. See
the URL below for further information:

  https://www.drupal.org/documentation/install/modules-themes/modules-5-6


TROUBLESHOOTING
---------------

 * Windows users: this module uses the link() system call to create
   hard links. This call was made available on Windows only on PHP
   5.3, so if you are using this OS, check your PHP version. Also, in
   Windows the link() function requires PHP to run in an elevated mode
   or with UAC disabled.

 * Due to the use of hard links to de-duplicate files, you may face
   problems if your files directory is in a file system that does not
   support hard links.



MAINTAINERS
-----------

Current maintainer:

 * Flávio Veloso (flaviovs) - https://drupal.org/u/flaviovs
