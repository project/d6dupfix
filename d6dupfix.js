$(document).ready(function () {

    var $cb = $('<input type="checkbox">');
    $cb
	.attr('id', 'd6dupfix-check-all')
	.attr('name', 'd6dupfix-check-all');

    var $label = $('<label>')
	.attr('for', 'd6dupfix-check-all')
	.text(Drupal.t('Select all'));

    var $div = $('<div>')
	.attr('id', 'd6dupfix-select-all')
	.append($cb)
	.append($label);

    $("#d6dupfix-form").prepend($div);

    $cb.change(function () {
	var $this = $(this);

	var $checkboxes = $("#d6dupfix-form fieldset input[type='checkbox'][id^='edit-fix-filepath-']");

	if ($this.is(':checked')) {
	    $checkboxes.attr("checked", "checked");
	}
	else {
	    $checkboxes.removeAttr("checked");
	}
    });
});